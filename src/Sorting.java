import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Sorting {
	private List<MonitoredData> monitoredData;

	   public Sorting(){
	       monitoredData = new ArrayList<>();
	       readFromFile();
	   }

	    public List<MonitoredData> getMonitoredData() {
	        return monitoredData;
	    }

		public void readFromFile(){
	        try(BufferedReader br = new BufferedReader(new FileReader("Activities.txt"))) {

	            String str;
	            DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	            while ((str=br.readLine()) != null) {
	            	
	                String[] str1 = str.split("\\s+");

	                LocalDateTime start = LocalDateTime.from(f.parse(str1[0] + " " + str1[1]));
	                LocalDateTime end = LocalDateTime.from(f.parse(str1[2] + " " + str1[3]));
	                String activity = str1[4];
	                
	                MonitoredData data = new MonitoredData();
	                data.setStartTime(start);
	                data.setEndTime(end);
	                data.setActivityLabel(activity);
	                
	                monitoredData.add(data);
	            }
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	    }

	    public void freq(){
	    	
	        Map<String, Long> count = monitoredData.stream().collect(Collectors.groupingBy(e -> e.getActivityLabel(), Collectors.counting()));
	       
	        try {
	        	
	            Files.write(Paths.get("Frequency.txt"), () -> count.entrySet().stream().<CharSequence>map(e -> e.getKey() + " " + e.getValue()).iterator());
	        
	        } catch (IOException e) {
	        	
	            e.printStackTrace();
	            
	        }
	        
	    }

	    public void filter(){
	    	
	        //Map<String, Long> count = monitoredData.stream().collect(Collectors.groupingBy(e -> e.getActivityLabel(), Collectors.counting()));        
//	        for(MonitoredData data:monitoredData){
//	        	
//	            if(map.containsKey(data.getActivityLabel())){
//	                map.put(data.getActivityLabel(), map.get(data.getActivityLabel()) +  ChronoUnit.SECONDS.between(data.getStartTime(), data.getEndTime()));
//	            }
//	            
//	            else{
//	                map.put(data.getActivityLabel(), ChronoUnit.SECONDS.between(data.getStartTime(), data.getEndTime()));
//	            }
//
//	        }
	        
	        //map = map.entrySet().stream().filter(e -> e.getValue() > 36000).collect(Collectors.toMap(e->e.getKey(),e->e.getValue()));;
	    	Map<String,Long> count = monitoredData.stream().collect(Collectors.groupingBy(e -> e.getActivityLabel(), Collectors.summingLong(e -> ChronoUnit.SECONDS.between(e.getStartTime(), e.getEndTime()))));
	    	Map<String,Long> m1 = count.entrySet().stream().filter(e -> e.getValue() > 36000).collect(Collectors.toMap(e->e.getKey(),e->e.getValue()));
	        
	        try {
	            Files.write(Paths.get("Filtered.txt"), () -> m1.entrySet().stream().<CharSequence>map(e -> e.getKey() + " " + e.getValue()/3600+" hrs").iterator());
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        
	    }

	    public void dayFreq(){
	    	
	        //Map<Integer,Integer> map = new HashMap<>();	        
//	        for(MonitoredData data:monitoredData){
//	        	
//	            Map<String, Long> day = monitoredData.stream().filter(e -> e.getStartTime().getDayOfMonth() == data.getStartTime().getDayOfMonth()).collect(Collectors.groupingBy(e -> e.getActivityLabel(), Collectors.counting()));
//	            
//	            if(!map.containsKey(data.getStartTime().getDayOfMonth())){
//	                map.put(data.getStartTime().getDayOfMonth(), day);
//	            }
//	            
//	        }
	    	Map<Object, Map<Object, Long>> count = monitoredData.stream().collect(Collectors.groupingBy(e -> e.getStartTime().getDayOfMonth(), Collectors.groupingBy(i -> i.getActivityLabel(), Collectors.counting())));

	        try {
	            Files.write(Paths.get("DayFrequency.txt"), () -> count.entrySet().stream().<CharSequence>map(e -> e.getKey() + " " + e.getValue()).iterator());
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        
	    }
}
