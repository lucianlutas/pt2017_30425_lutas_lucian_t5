import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

public class ui extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JPanel jPanel;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JScrollPane jScrollPane3;
    private JLabel lblDayFreq;
    private JLabel lblFilter;
    private JLabel lblFreq;
    public JTextArea txtDayFreq;
    public JTextField txtDistinct;
    public JTextArea txtFilter;
    public JTextArea txtFreq;

	public ui(long i){
		jPanel = new JPanel();
        jScrollPane3 = new JScrollPane();
        txtFreq = new JTextArea();
        jScrollPane2 = new JScrollPane();
        txtFilter = new JTextArea();
        jScrollPane1 = new JScrollPane();
        txtDayFreq = new JTextArea();
        lblFreq = new JLabel();
        lblFilter = new JLabel();
        lblDayFreq = new JLabel();
        txtDistinct = new JTextField();

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        txtFreq.setColumns(20);
        txtFreq.setRows(5);
        jScrollPane3.setViewportView(txtFreq);

        txtFilter.setColumns(20);
        txtFilter.setRows(5);
        jScrollPane2.setViewportView(txtFilter);

        txtDayFreq.setColumns(20);
        txtDayFreq.setRows(5);
        jScrollPane1.setViewportView(txtDayFreq);

        lblFreq.setText("Frequency");

        lblFilter.setText("Filtered");

        lblDayFreq.setText("Day Frequency");

        txtDistinct.setText("Number of distinct days");

        GroupLayout jPanelLayout = new GroupLayout(jPanel);
        jPanel.setLayout(jPanelLayout);
        jPanelLayout.setHorizontalGroup(
            jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, jPanelLayout.createSequentialGroup()
                .addGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelLayout.createSequentialGroup()
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 264, GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18))
                    .addGroup(jPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblFreq)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 264, GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblFilter))
                .addGap(18, 18, 18)
                .addGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(lblDayFreq)
                    .addComponent(jScrollPane3, GroupLayout.PREFERRED_SIZE, 264, GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25))
            .addGroup(jPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtDistinct)
                .addContainerGap())
        );
        jPanelLayout.setVerticalGroup(
            jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, jPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtDistinct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFreq)
                    .addComponent(lblFilter)
                    .addComponent(lblDayFreq))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane3, GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1, GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel, GroupLayout.PREFERRED_SIZE, 853, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        read(i);
        this.setVisible(true);
        this.setTitle("Stream Processing");
	}

	private void read(long i){
		try {
			FileReader r1 = new FileReader("Frequency.txt");
			FileReader r2 = new FileReader("DayFrequency.txt");
			FileReader r3 = new FileReader("Filtered.txt");
			
			BufferedReader br1 = new BufferedReader(r1);
			BufferedReader br2 = new BufferedReader(r2);	
			BufferedReader br3 = new BufferedReader(r3);
			
			txtDistinct.setText(i + " distinct days");
			txtDayFreq.read(br1, null);
			txtFilter.read(br3, null);
			txtFreq.read(br2, null);
			
			br1.close();
			br2.close();
			br3.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
