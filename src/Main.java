import java.util.List;

public class Main {
	public static void main(String a[]) {

        Sorting p = new Sorting();
        
        List<MonitoredData> date = p.getMonitoredData();
        long i = date.stream().map(data -> data.getStartTime().getDayOfMonth()).distinct().count();

        p.freq();
        p.filter();
        p.dayFreq();
        
        ui ui = new ui(i);

    }
}
