import java.time.LocalDateTime;

public class MonitoredData {
	private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activityLabel;

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }
    
    public String toString() {
        return "MonitoredData{startTime=" + startTime + ", endTime=" + endTime + ", activityLabel='" + activityLabel + '\'' + '}';
    }
}
